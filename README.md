# ABPUiHelper [![](https://jitpack.io/v/ABPSoft/ABPUiHelper.svg)](https://jitpack.io/#ABPSoft/ABPUiHelper)

The Android library to parse UI and change typeface.

## Usage
Add it in your root build.gradle at the end of repositories:

```gradle
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

Add the dependency

```gradle
dependencies {
    implementation 'com.github.ABPSoft:ABPUiHelper:1.2.15'
}
```



## License
[MIT](https://choosealicense.com/licenses/mit/)
